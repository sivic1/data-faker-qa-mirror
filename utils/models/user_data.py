class UserData:
    def __init__(self, company_name, full_name, email, mob_vehicle_id, vehicle_name, os_type, os_version, device_id,
                 mac_address, vin_number, eld_serial_number, latitude, longitude):
        self.company_name = company_name
        self.full_name = full_name
        self.email = email
        self.mob_vehicle_id = mob_vehicle_id
        self.vehicle_name = vehicle_name
        self.os_type = os_type
        self.os_version = os_version
        self.device_id = device_id
        self.mac_address = mac_address
        self.vin_number = vin_number
        self.eld_serial_number = eld_serial_number
        self.latitude = latitude
        self.longitude = longitude

    @classmethod
    def from_csv_line(cls, csv_line):
        data = [d.strip() for d in csv_line.split(',')]
        return cls(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9], data[10],
                   data[11], data[12])


def map_list(csv_file_list):
    data_list = []
    for data in csv_file_list:
        data_list.append(UserData.from_csv_line(data))
    return data_list
