from datetime import timezone, datetime
from json import JSONEncoder


class Alerts:
    def __init__(self, driver_status, eld_status='NOT_PAIRED'):
        self.created_date = str(datetime.now(timezone.utc))
        self.alerts = ['ELD_NOT_PAIRED']
        self.driver_state = driver_status
        self.eld_status = eld_status
        self.time_remaining = TimeRemaining()
        self.eld_data = ELDData()
        self.local_date_time = str(datetime.now(timezone.utc))

    def to_json(self):
        return dict(createdDate=self.created_date, alerts=self.alerts, driverState=self.driver_state,
                    eldStatus=self.eld_status, timeRemaining=self.time_remaining, eldData=self.eld_data,
                    localDateTime=self.local_date_time)


class TimeRemaining:
    def __init__(self, limit_duty=840.0, limit_drive=660.0, limit_cycle=3840.0, limit_rest=480.0,
                 limit_cycle_tomorrow=3840.0, time_until_shift_reset=1591087910497.0):
        self.limit_duty = limit_duty
        self.limit_drive = limit_drive
        self.limit_cycle = limit_cycle
        self.limit_rest = limit_rest
        self.limit_cycle_tomorrow = limit_cycle_tomorrow
        self.time_until_shift_reset = time_until_shift_reset

    def to_json(self):
        return dict(LIMIT_DUTY=self.limit_duty, LIMIT_DRIVE=self.limit_drive, LIMIT_CYCLE=self.limit_cycle,
                    LIMIT_REST=self.limit_rest, LIMIT_CYCLE_TOMORROW=self.limit_cycle_tomorrow,
                    TIME_UNTIL_SHIFT_RESET=self.time_until_shift_reset)


class ELDData:
    def __init__(self, engine_hours=-1, odometer=-1, speed=-1):
        self.engine_hours = engine_hours
        self.odometer = odometer
        self.speed = speed

    def to_json(self):
        return dict(engineHours=self.engine_hours, odometer=self.odometer, speed=self.speed)


class AlertsEncoder(JSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'to_json'):
            return obj.to_json()
        else:
            return JSONEncoder.default(self, obj)
