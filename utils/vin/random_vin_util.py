import os
import random

vin_digit_position_multiplier = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2]
vin_digit_values = {'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'J': 1,
                    'K': 2, 'L': 3, 'M': 4, 'N': 5, 'P': 7, 'R': 9, 'S': 2, 'T': 3, 'U': 4, 'V': 5,
                    'W': 6, 'X': 7, 'Y': 8, 'Z': 9, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6,
                    '7': 7, '8': 8, '9': 9, '0': 0}


class VinYear:
    def __init__(self, first8, year):
        self.First8 = first8
        self.Year = year

    def __repr__(self):
        return "First8: %s - Year: %s" % (self.First8, self.Year)


def get_random_vin():
    vin_year = get_random_vin_start()
    char = get_random_vin_char()

    v = "%s%s%s" % (vin_year.First8, char, vin_year.Year)
    for i in range(7):
        v += get_random_vin_char()

    check_char = get_check_sum_char(v)
    v = "%s%s%s" % (v[0:8], check_char, v[9:])
    return v


def get_check_sum_char(vin):
    check_sum_total = 0

    if len(vin) < 17:
        print("Invalid Length: %s" % len(vin))
        return -1

    for i in range(len(vin)):
        if vin_digit_values.get(vin[i], "-1") != "-1":
            check_sum_total += int(vin_digit_values[vin[i]]) * vin_digit_position_multiplier[i]
        else:
            print("Illegal Character: %s" % vin[i])
            return -1

    remain = check_sum_total % 11
    char = repr(remain)
    if remain == 10:
        char = 'X'

    return char


def get_random_vin_char():
    return random.choice(list(vin_digit_values.keys()))


def get_random_vin_start():
    base_path = os.path.dirname(__file__)
    vin_path = os.path.abspath(os.path.join(base_path, 'vin_data.txt'))
    vin_file = open(vin_path, 'r')
    count = 0
    line_to_read = int(random.random() * 62178)

    line = None
    try:
        while count <= line_to_read:
            line = vin_file.readline()
            count += 1
    finally:
        vin_file.close()

    fields = line.split()
    return VinYear(fields[0].strip(), fields[1].strip())


def is_valid_vin(vin):
    if len(vin) != 17:
        return False
    c = get_check_sum_char(vin)

    return c == vin[8]
