from datetime import timedelta, datetime
from json import JSONEncoder, dumps

import dateutil.tz

from utils import random_utils as random_util

seq_id = -1


def sequence():
    global seq_id
    seq_id += 1
    return seq_id


def initial_logs_json(company_name, driver_name, vehicle_id, vehicle_name, device_id, device_mac_address,
                      trailer=random_util.number(1000, 99999)):
    logs = []
    for dm in range(8):
        midnight = ((datetime
                     .now(dateutil.tz.gettz('America/Chicago')) - timedelta(days=dm))
                    .replace(hour=0, minute=0, second=0, microsecond=0)).timestamp() * 1000
        logs.append(
            DailyLog(company_name, driver_name, vehicle_id, vehicle_name, device_id, device_mac_address, midnight))
    for log in logs:
        log.trailers = trailer

    return dumps(logs, cls=DailyLogEncoder)


def get_latest_log(logs):
    latest_log = None
    for log in logs:
        if latest_log is None:
            latest_log = log
        if log.date_midnight > latest_log.date_midnight:
            latest_log = log

    return latest_log


class DailyLogEncoder(JSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'to_json'):
            return obj.to_json()
        else:
            return JSONEncoder.default(self, obj)


class DailyLog:
    def __init__(self, carrier_name, driver_name, current_vehicle_id, current_vehicle_name, device_id,
                 eld_auth_value, date_midnight,
                 pending=False, approved_reassign=False, co_driver_approved_reassign=False, signed=False,
                 annotation='', assigner_name=None, assigner_role=None, city=None, co_drivers=None,
                 cycle_rule='USA_70_8', daily_log_entries=None, daily_log_notes=None, daily_log_values=None,
                 violations=None, time_zone='central', time_zone_offset_from_utc=-6, device_type='ELD',
                 device_vin=None, distance=0.0, from_location='', external_id=None, log_signing_location=None,
                 main_office='Company Name {}'.format('Fake Address'), notes=random_util.string(25),
                 shipping_docs=random_util.number(99, 9999), state='NY', hours_worked=0.0,
                 terminal_location='Company Name {}'.format('Fake Address'), to_location='',
                 trailers=random_util.number(99, 9999), all_vehicle_ids=None, all_vehicle_names=None,
                 adverse_driving=False, driver_id=None, cycle_reset_occurred_time=None, void_if_needed=False):

        if violations is None:
            violations = []

        if co_drivers is None:
            co_drivers = []

        if daily_log_entries is None:
            today = ((datetime
                      .now(dateutil.tz.gettz('America/Chicago')) - timedelta(days=0))
                     .replace(hour=0, minute=0, second=0, microsecond=0)).timestamp() * 1000
            if today == date_midnight:
                midnight = ((datetime
                             .now(dateutil.tz.gettz('America/Chicago')) - timedelta(days=0)))
                end_minutes = midnight.hour * 60 + midnight.minute
                daily_log_entries = [
                    DailyLogEntry(device_id=device_id, vehicle_id=current_vehicle_id, end_time=end_minutes,
                                  midnight_millis=date_midnight)]
            else:
                daily_log_entries = [
                    DailyLogEntry(device_id=device_id, vehicle_id=current_vehicle_id, midnight_millis=date_midnight)]

        if daily_log_notes is None:
            daily_log_notes = []

        if daily_log_values is None:
            today = ((datetime
                      .now(dateutil.tz.gettz('America/Chicago')) - timedelta(days=0))
                     .replace(hour=0, minute=0, second=0, microsecond=0)).timestamp() * 1000
            if today == date_midnight:
                midnight = ((datetime
                             .now(dateutil.tz.gettz('America/Chicago')) - timedelta(days=0)))
                current_minutes = midnight.hour * 60 + midnight.minute
                daily_log_values = [
                    DailyLogValue(device_id, time=current_minutes, value_type='ELD_CONNECTED', value=True,
                                  midnight_millis=date_midnight)]
            else:
                daily_log_values = []

        if all_vehicle_ids is None:
            all_vehicle_ids = [current_vehicle_id]

        if all_vehicle_names is None:
            all_vehicle_names = [current_vehicle_name]

        self.pending = pending
        self.approved_reassign = approved_reassign
        self.co_driver_approved_reassign = co_driver_approved_reassign
        self.signed = signed
        self.annotation = annotation
        self.assigner_name = assigner_name
        self.assigner_role = assigner_role
        self.carrier_name = carrier_name
        self.city = city
        self.co_drivers = co_drivers
        self.cycle_rule = cycle_rule
        self.daily_log_entries = daily_log_entries
        self.daily_log_notes = daily_log_notes
        self.daily_log_values = daily_log_values
        self.violations = violations
        self.date_midnight = date_midnight
        self.time_zone = time_zone
        self.time_zone_offset_from_utc = time_zone_offset_from_utc
        self.device_type = device_type
        self.device_vin = device_vin
        self.distance = distance
        self.driver_id = driver_id
        self.driver_name = driver_name
        self.eld_auth_value = eld_auth_value
        self.external_id = external_id
        self.from_location = from_location
        self.log_signing_location = log_signing_location
        self.main_office = main_office
        self.notes = notes
        self.shipping_docs = shipping_docs
        self.state = state
        self.hours_worked = hours_worked
        self.terminal_location = terminal_location
        self.to_location = to_location
        self.trailers = trailers
        self.current_vehicle_id = current_vehicle_id
        self.all_vehicle_ids = all_vehicle_ids
        self.current_vehicle_name = current_vehicle_name
        self.all_vehicle_names = all_vehicle_names
        self.adverse_driving = adverse_driving
        self.cycle_reset_occurred_time = cycle_reset_occurred_time
        self.void_if_needed = void_if_needed

    def to_json(self):
        return dict(pending=self.pending, approvedReassign=self.approved_reassign,
                    coDriverApprovedReassign=self.co_driver_approved_reassign, signed=self.signed,
                    annotation=self.annotation, assignerName=self.assigner_name, assignerRole=self.assigner_role,
                    carrierName=self.carrier_name, city=self.city, coDrivers=self.co_drivers,
                    cycleRule=self.cycle_rule, dailyLogEntries=self.daily_log_entries,
                    dailyLogNotes=self.daily_log_notes, dailyLogValues=self.daily_log_values,
                    violations=self.violations, dateMidnight=self.date_midnight, timeZone=self.time_zone,
                    timeZoneOffsetFromUtc=self.time_zone_offset_from_utc, deviceType=self.device_type,
                    deviceVin=self.device_vin, distance=self.distance, driverId=self.driver_id,
                    driverName=self.driver_name, eldAuthValue=self.eld_auth_value, externalId=self.external_id,
                    fromLocation=self.from_location, logSigningLocation=self.log_signing_location,
                    mainOffice=self.main_office, notes=self.notes, shippingDocs=self.shipping_docs, state=self.state,
                    hoursWorked=self.hours_worked, terminalLocation=self.terminal_location, toLocation=self.to_location,
                    trailers=self.trailers, currentVehicleId=self.current_vehicle_id,
                    allVehicleIds=self.all_vehicle_ids, currentVehicleName=self.current_vehicle_name,
                    allVehicleNames=self.all_vehicle_names, adverseDriving=self.adverse_driving,
                    cycleResetOccurredTime=self.cycle_reset_occurred_time, voidIfNeeded=self.void_if_needed)


class DailyLogEntry:
    def __init__(self, vehicle_id, device_id, distance=0.0,
                 driver_state='OFF_DUTY', end_engine_hours=0.0, engine_hours=0.0, event_id=None, from_eld=False,
                 location='', longitude=-87.6144183, latitude=41.5842577, location_entry_type='AUTO', note='',
                 odometer=0.0, read_only=False, start_time=0, end_time=1440, edited=False, auto_switched=False,
                 midnight_millis=0):
        self.sequence_id = sequence()
        self.device_id = device_id
        self.distance = distance
        self.driver_state = driver_state
        self.end_engine_hours = end_engine_hours
        self.engine_hours = engine_hours
        self.event_id = event_id
        self.from_eld = from_eld
        self.location = location
        self.longitude = longitude
        self.latitude = latitude
        self.location_entry_type = location_entry_type
        self.note = note
        self.odometer = odometer
        self.read_only = read_only
        self.start_time = start_time
        self.end_time = end_time
        self.vehicle_id = vehicle_id
        self.edited = edited
        self.auto_switched = auto_switched
        self.start_millis = midnight_millis + self.start_time * 60 * 1000
        self.end_millis = midnight_millis + self.end_time * 60 * 1000

    def to_json(self):
        return dict(sequenceId=self.sequence_id, deviceId=self.device_id, distance=self.distance,
                    driverState=self.driver_state, endEngineHours=self.end_engine_hours,
                    engineHours=self.engine_hours,
                    eventId=self.event_id, fromEld=self.from_eld, location=self.location, longitude=self.longitude,
                    latitude=self.latitude, locationEntryType=self.location_entry_type, note=self.note,
                    odometer=self.odometer, readOnly=self.read_only, startTime=self.start_time,
                    endTime=self.end_time, startMillis=self.start_millis, endMillis=self.end_millis,
                    vehicleId=self.vehicle_id, edited=self.edited, autoSwitched=self.auto_switched)


class DailyLogNote:
    def __init__(self, location=None, note=None, time=None):
        self.location = location
        self.note = note
        self.time = time

    def to_json(self):
        return dict(location=self.location, note=self.note, time=self.time)


class DailyLogValue:
    def __init__(self, device_id, coordinates=None, driver_state='OFF_DUTY',
                 engine_hours=0.0, event_id=None, location='', location_lat_new=None, location_lon_new=None,
                 odometer=0.0, time=None, value_type=None, value=None, midnight_millis=0):
        self.sequence_id = sequence()
        self.device_id = device_id
        self.coordinates = coordinates
        self.driver_state = driver_state
        self.engine_hours = engine_hours
        self.event_id = event_id
        self.location = location
        self.location_lat_new = location_lat_new
        self.location_lon_new = location_lon_new
        self.location_entry_type = 'AUTO'
        self.odometer = odometer
        self.time = time
        self.value_type = value_type
        self.value = value
        self.time_millis = midnight_millis + time * 60 * 1000

    def to_json(self):
        return dict(sequenceId=self.sequence_id, deviceId=self.device_id, coordinates=self.coordinates,
                    driverState=self.driver_state, engineHours=self.engine_hours, eventId=self.event_id,
                    location=self.location, locationLatNew=self.location_lat_new, locationLonNew=self.location_lon_new,
                    locationEntryType=self.location_entry_type, odometer=self.odometer, time=self.time,
                    type=self.value_type, value=self.value, timeMillis=self.time_millis)
