from json import JSONEncoder
import datetime
from utils import random_utils as rutil


class AnyTrekDataEncoder(JSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'to_json'):
            return obj.to_json()
        else:
            return JSONEncoder.default(self, obj)


class AnyTrekData:
    def __init__(self, gps_sn, latitude, longitude, utc_time=0):
        self.c = 'uRpLoc'
        self.id = gps_sn
        self.t = str(datetime.datetime.utcnow().strftime('%Y%m%d%H%M%S')) if utc_time == 0 else str(utc_time)
        self.sta = AnyTrekStatusModel(rutil.number(1, 100), rutil.number(-50, 50), True, True)
        self.gps = AnyTrekGPSModel(latitude, longitude, rutil.number(0, 75), rutil.number(0, 360))
        self.statistic = AnyTrekStatisticModel(1, rutil.number(10, 500))

    def to_json(self):
        return dict(c=self.c, id=self.id, t=self.t, sta=self.sta, gps=self.gps, statistic=self.statistic)


class AnyTrekGPSModel:
    def __init__(self, latitude, longitude, speed, degrees):
        self.latitude = latitude
        self.longitude = longitude
        self.speed = speed
        self.degrees = degrees

    def to_json(self):
        return dict(lat=float(self.latitude), lng=float(self.longitude), spd=self.speed, hdg=self.degrees)


class AnyTrekStatusModel:
    def __init__(self, battery_level, temperature, charging, acc):
        self.battery_level = battery_level
        self.temperature = temperature
        self.charging = 1 if charging else 0
        self.acc = 1 if acc else 0

    def to_json(self):
        return dict(bat=self.battery_level, charging=self.charging, temperature=self.temperature, acc=self.acc)


class AnyTrekStatisticModel:
    def __init__(self, original_event, mileage):
        self.original_event = original_event
        self.mileage = int(mileage * 1609.34)

    def to_json(self):
        return dict(originalEvent=self.original_event, mileage=self.mileage)
