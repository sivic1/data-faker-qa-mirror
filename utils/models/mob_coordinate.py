from json import JSONEncoder


class MobCoordinate:
    def __init__(self, status, driver_id, vehicle_id):
        self.driver_status = MobDriverStatus(status, driver_id, vehicle_id)
        self.odometer_distance = None
        self.positions = []

    def to_json(self):
        return dict(driverStatus=self.driver_status, odometerDistance=self.odometer_distance, positions=self.positions)


class MobPosition:
    def __init__(self, driver_id, vehicle_id, status, geo_data):
        self.geocoded_location = None
        self.date = 0
        self.day = 1
        self.distance = 0
        self.driver_id = driver_id
        self.driver_state = status
        self.eld_status = 'CONNECTED'
        self.engine_hours = geo_data.engine_hours
        self.location_new = geo_data.latitude
        self.location_old = geo_data.longitude
        self.odometer = geo_data.odometer
        self.us_state = 'US_NY'
        self.vehicle_id = vehicle_id
        self.speed = geo_data.speed
        self.bearing = geo_data.direction_degrees

    def to_json(self):
        return dict(geocodedLocation=self.geocoded_location, date=self.date, day=self.day, distance=self.distance,
                    driverId=self.driver_id, driverState=self.driver_state, eldStatus=self.eld_status,
                    engineHours=self.engine_hours, locationNew=self.location_new, locationOld=self.location_old,
                    odometer=self.odometer, usState=self.us_state, vehicleId=self.vehicle_id, speed=self.speed,
                    bearing=self.bearing)


class MobDriverStatus:
    def __init__(self, status, driver_id, vehicle_id, cycle_rule='USA_70_8', cycle_left_for_today=3358,
                 cycle_left_for_tomorrow=3358, drive_time_remaining=-2, on_duty_time_remaining=-2,
                 time_until_break=-362, time_until_shift_reset=1591121251421):
        self.current_status = status
        self.cycle_rule = cycle_rule
        self.cycle_left_for_today = cycle_left_for_today
        self.cycle_left_for_tomorrow = cycle_left_for_tomorrow
        self.drive_time_remaining = drive_time_remaining
        self.driver_id = driver_id
        self.on_duty_time_remaining = on_duty_time_remaining
        self.time_until_break = time_until_break
        self.time_until_shift_reset = time_until_shift_reset
        self.engine_status = str(0)
        self.unit_id = vehicle_id

    def to_json(self):
        return dict(currentStatus=self.current_status, cycleRule=self.cycle_rule, engineStatus=self.engine_status,
                    unitId=self.unit_id, driverId=self.driver_id, cycleLeftForToday=self.cycle_left_for_today,
                    cycleLeftForTomorrow=self.cycle_left_for_tomorrow, driveTimeRemaining=self.drive_time_remaining,
                    onDutyTimeRemaining=self.on_duty_time_remaining, timeUntilBreak=self.time_until_break,
                    timeUntilShiftReset=self.time_until_shift_reset)


class MobCoordinatesEncoder(JSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'to_json'):
            return obj.to_json()
        else:
            return JSONEncoder.default(self, obj)
