class VehicleData:
    def __init__(self, vin, eld_sn, latitude, longitude):
        self.vin = vin
        self.eld_sn = eld_sn
        self.latitude = latitude
        self.longitude = longitude
