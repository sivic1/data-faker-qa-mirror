from json import JSONEncoder

from utils import random_utils as rutil


class GeometrisDataEncoder(JSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'to_json'):
            return obj.to_json()
        else:
            return JSONEncoder.default(self, obj)


class GeometrisData:
    def __init__(self, packet_time_stamp, event_date_time, obd_vin, identifier, latitude, longitude,
                 message_reason='ON_PERIODIC', faults=None):
        if faults is None:
            faults = []

        if rutil.number(1, 11) % 5 != 0:
            self.speed = rutil.number(5, 100)
        else:
            self.speed = 0

        self.measurements = 'English'
        self.format = 'F01F'
        self.identifier = identifier
        self.message_reason = message_reason
        self.packet_time_stamp = packet_time_stamp
        self.event_date_time = event_date_time
        self.location = GeometrisLocation(latitude, longitude)
        self.unique_id = rutil.number(1000)
        self.location_age_min = 0
        self.operating_states = None
        self.duration = 0
        self.direction_degrees = rutil.number(0, 360)
        self.odometer = rutil.number(10000, 500000)
        self.num_satellites = rutil.number(1, 15)
        self.idle_duration = 0
        self.obd_speed = self.speed
        self.obd_odometer = self.odometer
        self.obd_vin = obd_vin
        self.obd_fuel = None if message_reason == 'POWER_CUT' else rutil.number(5, 100)
        self.obd_throttle = rutil.number(0, 2000)
        self.obd_mpg = rutil.number(0, 100)
        self.obd_trip_mpg = rutil.number(0, 100)
        self.obd_instant_mpg = rutil.number(0, 100)
        self.engine_hours = rutil.number(1000, 100000) * 100
        self.total_fuel_used = rutil.number(50, 100)
        self.faults = faults

    def to_json(self):
        geo_list = []
        obj = dict(Measurements=self.measurements, Format=self.format, Identifier=self.identifier,
                   MessageReason=self.message_reason, PacketTimeStamp=self.packet_time_stamp,
                   EventDateTime=self.event_date_time, Location=self.location, UniqueId=self.unique_id,
                   LocationAgeMin=self.location_age_min, OperatinStates=self.operating_states, Duration=self.duration,
                   Speed=self.speed, DirectionDegrees=self.direction_degrees, Odometer=self.odometer,
                   NumSatellites=self.num_satellites, IdleDuration=self.idle_duration, OBDSpeed=self.obd_speed,
                   OBDOdometer=self.obd_odometer, OBDVIN=self.obd_vin, OBDFuel=self.obd_fuel,
                   OBDThrottle=self.obd_throttle, OBDMPG=self.obd_mpg, OBDTripMPG=self.obd_trip_mpg,
                   OBDInstantMPG=self.obd_instant_mpg, J1939EngineHours=self.engine_hours,
                   J1939TotalFuelUsed=self.total_fuel_used, DiagnosticCodes=self.faults)
        geo_list.append(obj)
        return geo_list


class GeometrisLocation:
    def __init__(self, latitude, longitude, named_location_type=0, named=False, interpolated=False, named_location=None,
                 address=None, cross_street=None, region_title=None, region_id='00000000-0000-0000-0000-000000000000'):
        self.named_location_type = named_location_type
        self.named = named
        self.interpolated = interpolated
        self.geo_location = GeometrisGeoLocation(latitude, longitude)
        self.named_location = named_location
        self.address = address
        self.cross_street = cross_street
        self.region_title = region_title
        self.region_id = region_id

    def to_json(self):
        return dict(NamedLocationType=self.named_location_type, Named=self.named, Interpolated=self.interpolated,
                    GeoLocation=self.geo_location, NamedLocation=self.named_location, Address=self.address,
                    CrossStreet=self.cross_street, RegionTitle=self.region_title, ReqionId=self.region_id)


class GeometrisGeoLocation:
    def __init__(self, latitude, longitude, altitude=0, geodetic_cell=None, geodetic_fine=None):
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.geodetic_cell = geodetic_cell
        self.geodetic_fine = geodetic_fine

    def to_json(self):
        return dict(Latitude=self.latitude, Longitude=self.longitude, Altitude=self.altitude,
                    GeodeticCell=self.geodetic_cell, GeodeticFine=self.geodetic_fine)


class GeometrisNamedLocation:
    def __init__(self, latitude, longitude, altitude, geodetic_cell, geodetic_fine):
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.geodetic_cell = geodetic_cell
        self.geodetic_fine = geodetic_fine

    def to_json(self):
        return dict(Latitude=self.latitude, Longitude=self.longitude, Altitude=self.altitude,
                    GeodeticCell=self.geodetic_cell, GeodeticFine=self.geodetic_fine)


class GeometrisAddress:
    def __init__(self, address_line_1, address_line_2, building, city, country_region, floor_level, postal_code,
                 state_province, street_name, is_displaced, displacement_amount, displacement_direction):
        self.address_line_1 = address_line_1
        self.address_line_2 = address_line_2
        self.building = building
        self.city = city
        self.country_region = country_region
        self.floor_level = floor_level
        self.postal_code = postal_code
        self.state_province = state_province
        self.street_name = street_name
        self.is_displaced = is_displaced
        self.displacement_amount = displacement_amount
        self.displacement_direction = displacement_direction

    def to_json(self):
        return dict(AddressLine1=self.address_line_1, AddressLine2=self.address_line_2, Building=self.building,
                    City=self.city, CountryRegion=self.country_region, FloorLevel=self.floor_level,
                    PostalCode=self.postal_code, StateProvince=self.state_province,
                    StreetName=self.street_name, IsDisplaced=self.is_displaced,
                    DisplacementAmount=self.displacement_amount, DisplacementDirection=self.displacement_direction)
