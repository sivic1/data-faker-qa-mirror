import json
import random
import string as strg
from datetime import datetime, timezone, timedelta
from random import Random, choice
from faker import Faker

from utils.vin import random_vin_util

base_email = ''
fake = Faker()


def milliseconds(days=0, hours=0, plus_days=0, plus_hours=0):
    now = datetime.now(timezone.utc) - timedelta(days=days) - timedelta(hours=hours) + timedelta(
        days=plus_days) + timedelta(hours=plus_hours)
    epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
    posix_timestamp_micros = (now - epoch) // timedelta(microseconds=1)
    posix_timestamp_millis = posix_timestamp_micros // 1000

    return posix_timestamp_millis


def vin():
    return random_vin_util.get_random_vin()


def string(size=8, chars=strg.ascii_lowercase):
    r = Random()
    return ''.join(r.choice(chars) for _ in range(size))


def number(start=0, end=10000000000):
    r = Random()
    return r.randrange(start, end)


def mac():
    r = Random()
    return '{}:{}:{}:{}:{}:{}'.format(str(r.randint(10, 99)),
                                      str(r.randint(10, 99)),
                                      str(r.randint(10, 99)),
                                      str(r.randint(10, 99)),
                                      str(r.randint(10, 99)),
                                      str(r.randint(10, 99)))


def eld():
    r = Random()
    return 'ELDX' + str(r.randint(10000000, 99999999))


def gps_sn():
    r = Random()
    return r.randint(10000000000000, 99999999999999)


def gps_model():
    r = Random()
    return r.choice(['round1611', 'oval1711'])  # 'thermo1802',


def read_only():
    r = Random()
    return r.choice(['true', 'false'])


class Company:
    def __init__(self, super_admin_id, email, company_name=None, first_name=fake.first_name(),
                 last_name=fake.last_name(), phone_number='064' + str(random.randint(999999, 9999999)),
                 adding_companies_allowed=choice([True, False]), dot_number=None, billing_status='active',
                 address=fake.address(), city=fake.city(), state='US_NY', postal_code=10008,
                 price_per_unit=number(10, 250), contract_num=number(9999), contract_start=milliseconds(days=1),
                 contract_end=milliseconds(plus_days=365), time_zone='central', dispatch_board_available=True,
                 transfer_data_available=True, eld_permissions=True, advanced_settings_allowed=True,
                 monitoring_allowed=True,
                 billing_start=milliseconds(days=1)):
        self.super_admin_id = super_admin_id
        self.email = email
        if company_name is None:
            self.company_name = string(10).capitalize()
        else:
            self.company_name = company_name
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        self.adding_companies_allowed = adding_companies_allowed
        self.address = address
        self.city = city
        self.state = state
        self.postal_code = postal_code
        if not dot_number:
            self.dot_number = number(end=99999)
        self.price_per_unit = price_per_unit
        self.contract_num = contract_num
        self.contract_start = contract_start
        self.contract_end = contract_end
        self.time_zone = time_zone

        self.dispatch_board_available = dispatch_board_available
        self.transfer_data_available = transfer_data_available
        self.advanced_settings_allowed = advanced_settings_allowed
        self.monitoring_allowed = monitoring_allowed
        self.eld_permissions = eld_permissions

        self.billing_status = billing_status
        self.billing_start = billing_start

    def json(self):
        return json.dumps({'firstName': self.first_name, 'lastName': self.last_name, 'phoneNum': self.phone_number,
                           'email': self.email, 'emailConfirmation': self.email,
                           'addingCompaniesAllowed': self.adding_companies_allowed, 'companyName': self.company_name,
                           'dotNum': self.dot_number, 'address': self.address, 'city': self.city, 'state': self.state,
                           'postalCode': self.postal_code, 'pricePerUnit': self.price_per_unit,
                           'contractNum': self.contract_num, 'contractStart': self.contract_start,
                           'contractEnd': self.contract_end, 'superAdminId': self.super_admin_id,
                           'homeTerminalTimeZone': self.time_zone, 'billing_start': self.billing_start,
                           'dispatchBoardAvailable': self.dispatch_board_available,
                           'transferDataAvailable': self.transfer_data_available,
                           'advancedSettingsAllowed': self.advanced_settings_allowed,
                           'monitoringAllowed': self.monitoring_allowed, "eldPermissions": self.eld_permissions,
                           'billingStatusId': self.billing_status})


class Driver:
    def __init__(self, company_id):
        self.first_name = fake.first_name()
        self.last_name = fake.last_name()
        self.full_name = '{} {}'.format(self.first_name, self.last_name)
        self.email = '{}{}{}@lioneight.com'.format(base_email, self.first_name.lower(), self.last_name.lower())
        self.phone_number = '066{}'.format(string(size=7, chars=strg.digits))
        self.licence_number = string(size=6, chars=strg.digits)
        self.exempt = False
        self.yard = True
        self.pc = True
        self.manual = True
        self.enhance_sound = False
        self.company_id = company_id
        self.street = '{} {}'.format(string().capitalize(), string().capitalize())
        self.city = string(size=10).capitalize()
        self.state = 'US_NY'
        self.postal_code = '10008'
        self.home_time_zone = 'central'
        self.odometer = random.choice(['km', 'mi'])
        self.driverType = 'companyDriver'
        self.cycle_rule = {'cycleRuleType': '70hour', 'cargoType': 'property', 'restBreakException': False,
                           'twentyFourHourCycleReset': False}

    def json(self):
        return json.dumps(
            {'firstName': self.first_name, 'lastName': self.last_name, 'driverId': '', 'email': self.email,
             'phoneNum': self.phone_number, 'licenseState': self.state, 'licenseNum': self.licence_number,
             'exemptFromEld': self.exempt, 'allowYardMove': self.yard, 'allowPersonalConveyance': self.pc,
             'allowManualDriveTime': self.manual, 'enhanceViolationSoundAlert': self.enhance_sound,
             'companyId': self.company_id, 'street': self.street, 'city': self.city, 'state': self.state,
             'postalCode': self.postal_code, 'homeTerminalTimeZone': self.home_time_zone,
             'odometerUnits': self.odometer, 'driverType': self.driverType, 'cycleRule': self.cycle_rule})


class Vehicle:
    def __init__(self, company_id, dvir_form_id, eld_id, gps_id=''):
        self.vehicle_id = 'V{}'.format(string(size=8, chars=strg.digits))
        self.vin = vin()
        self.year = number(2000, 2020)
        self.make = string(size=10).capitalize()
        self.model = string(size=6).capitalize()
        self.fuel_type = random.choice(['diesel', 'gasoline', 'propane', 'liquidNG', 'compressedNG', 'ethanol',
                                        'methanol', 'm83', 'm85', 'a55', 'biodiesel', 'other'])
        self.state = 'US_NY'
        self.licence_number = string(size=5, chars=strg.digits)
        self.odometer = random.choice(['km', 'mi'])
        self.inspection_form = 'usReport'
        self.company_id = company_id
        self.eld_id = eld_id
        self.gps_id = gps_id
        self.dvir_form_id = dvir_form_id

    def json(self):
        return json.dumps(
            {'vehicleId': self.vehicle_id, 'vin': self.vin, 'year': self.year, 'make': self.make,
             'model': self.model, 'fuelType': self.fuel_type, 'state': self.state, 'licenseNum': self.licence_number,
             'odometerUnit': self.odometer, 'inspectionForm': self.inspection_form, 'companyId': self.company_id,
             'eldId': self.eld_id, 'gpsId': self.gps_id, 'dvirFormId': self.dvir_form_id})


class Trailer:
    def __init__(self, company_id, gps_id=''):
        self.trailer_id = 'T{}'.format(string(size=8, chars=strg.digits))
        self.type = choice(['dryVan', 'reefer', 'flatbed', 'po'])
        self.make = string(size=10).capitalize()
        self.year = number(2000, 2020)
        self.vin = vin()
        self.plates = '{}-{}'.format(choice(['NY', 'WA', 'IL', 'CA', 'MN']), string(size=5, chars=strg.digits))
        self.ownership = choice(['company', 'leased', 'contractors'])
        self.suspension = choice(['air', 'spring'])
        self.company_id = company_id
        self.gps_id = gps_id

    def json(self):
        return json.dumps(
            {'trailerName': self.trailer_id, 'type': self.type, 'make': self.make, 'year': self.year, 'vin': self.vin,
             'plates': self.plates, 'ownership': self.ownership, 'suspension': self.suspension,
             'companyId': self.company_id, 'gpsId': self.gps_id})
