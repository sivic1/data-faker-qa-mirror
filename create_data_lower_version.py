import base64
import json
import os
import random
import string
import sys
from datetime import datetime, timezone, timedelta
from queue import Queue
from random import shuffle
from time import sleep
import multiprocessing
import requests

from utils import random_utils_lower_version as rutil
from utils.models import daily_log
from utils.models.any_trek_data import AnyTrekData, AnyTrekDataEncoder
from utils.models.geometris_data import GeometrisData, GeometrisDataEncoder
from utils.random_utils import Company


def login(app_url, username='boskokg@gmail.com'):
    _headers = {'content-type': 'application/json'}
    login_data = {'username': username, 'password': 'seve'}
    _r = requests.post('{}/web/login'.format(app_url), headers=_headers, data=json.dumps(login_data))
    token = _r.json()['token']
    _headers['Authorization'] = 'Bearer ' + token

    return _headers


def create_elds_and_gpss(app_url, company_name, count=1):
    _headers = login(app_url)
    ids = []
    gpss = []
    manufacturer = "geometris"
    for _ in range(count):
        eld_serial_num = rutil.eld()
        eld_mac_add = rutil.mac()

        extras_eld = requests.get('{}/web/superAdmin/eld'.format(app_url), headers=_headers)
        com_id = None
        for comp in extras_eld.json()['extras']['companies']:
            if comp['companyName'] == company_name:
                com_id = comp['companyId']
                break
        if com_id is None:
            print('Cannot find company by name {}!'.format(company_name))
            exit(100)

        requests.post('{}/web/superAdmin/eld'.format(app_url), headers=_headers,
                      data=json.dumps({'companyId': com_id,
                                       'manufacturer': manufacturer,
                                       'macAddress': eld_mac_add,
                                       'serialNum': eld_serial_num,
                                       'status': 'active'}))

        parameters_elds = {'page': '1', 'elements': '2000', "orderBy": "serialNum", "manufacturer": "geometris", "asc": "true", "startTime": "0", "endTime": "2643587200000"}
        search_elds_response = requests.get('{}/web/superAdmin/elds'.format(app_url),
                                            params=parameters_elds, headers=_headers)

        for _eld in search_elds_response.json()['data']:
            if _eld['serialNum'] == eld_serial_num:
                ids.append((_eld['id'], eld_serial_num, eld_mac_add))

        gps_serial_num = rutil.gps_sn()
        gps_model = rutil.gps_model()

        requests.post('{}/web/superAdmin/gps'.format(app_url), headers=_headers,
                      data=json.dumps({'companyId': com_id,
                                       'serialNum': gps_serial_num,
                                       'modelId': gps_model,
                                       'status': 'active'}))

        parameters_gpss = {'page': '1', 'elements': '2000', "orderBy": "serialNum", "asc": "true", "startTime": "0", "endTime": "2643587200000"}
        search_gpss_response = requests.get('{}/web/superAdmin/gpss'.format(app_url),
                                            params=parameters_gpss, headers=_headers)

        for _gps in search_gpss_response.json()['data']:
            if _gps['serialNum'] == gps_serial_num:
                gpss.append((_gps['id'], gps_serial_num, gps_model))

        gps_trailer_serial_num = rutil.gps_sn()
        requests.post('{}/web/superAdmin/gps'.format(app_url), headers=_headers,
                      data=json.dumps({'companyId': com_id,
                                       'serialNum': gps_trailer_serial_num,
                                       'modelId': 'thermo1802',
                                       'status': 'active'}))

    requests.post('{}/web/logout'.format(app_url), headers=_headers)
    return ids, gpss


def create_data(data):
    app_url = data[0]
    c = data[1]
    drivers = []
    headers = login(app_url)
    parameters = {'page': '1', 'elements': '2000', "orderBy": "name", "asc": "true"}
    search_super_admins_response = requests.get('{}/web/superAdmin/admins'.format(app_url), params=parameters,
                                                headers=headers)

    sa_id = None
    for sa in search_super_admins_response.json()['data']:
        if sa['email'] == 'boskokg@gmail.com':
            sa_id = sa['id']
            break
    com = Company(sa_id, c[1], c[0])
    create_company_response = requests.post('{}/web/superAdmin/company'.format(app_url), headers=headers,
                                            data=com.json())
    if create_company_response.status_code != 200:
        print('Cannot create company {}.\nReason: {}!'.format(c[0],
                                                              create_company_response.json()['errors'][0]['message']))
        return
    else:
        print('Company {} created!'.format(com.company_name))
    devices = create_elds_and_gpss(app_url, c[0], c[2])
    elds = devices[0]
    # gpss = devices[1]

    headers = login(app_url, c[1])
    driver = None
    for _ in range(c[2]):
        extras_driver = requests.get('{}/web/fleetManagement/driver'.format(app_url), headers=headers)
        company_id = extras_driver.json()['extras']['companies'][0]['id']

        driver = rutil.Driver(company_id)
        requests.post('{}/web/fleetManagement/driver'.format(app_url), headers=headers, data=driver.json())

        extras_vehicle = requests.get('{}/web/fleetManagement/vehicle'.format(app_url), headers=headers)
        eld_id = extras_vehicle.json()['extras']['companies'][0]['elds'][0]['id']

        gps_id = extras_vehicle.json()['extras']['companies'][0]['gpsDevices'][0]['id']
        gps_sn = extras_vehicle.json()['extras']['companies'][0]['gpsDevices'][0]['serialNum']
        eld_sn = extras_vehicle.json()['extras']['companies'][0]['elds'][0]['serialNum']
        eld_mac = None
        for eld in elds:
            if eld[1] == eld_sn:
                eld_mac = eld[2]

        # vehicles
        vehicle = rutil.Vehicle(company_id, eld_id, gps_id=gps_id)
        requests.post('{}/web/fleetManagement/vehicle'.format(app_url), headers=headers, data=vehicle.json())

        # trailers
        extras_trailers = requests.get('{}/web/fleetManagement/trailer'.format(app_url), headers=headers)
        trailer_gpss = []
        for t in extras_trailers.json()['extras']['companies'][0]['gpsDevices']:
            if t['modelId'] == 'thermo1802':
                trailer_gpss.append((t['id'], t['serialNum']))
        random_trailer_gps = random.choice(trailer_gpss)
        trailer = rutil.Trailer(company_id, random_trailer_gps[0])
        requests.post('{}/web/fleetManagement/trailer'.format(app_url), headers=headers, data=trailer.json())

        drivers.append(
            (driver.email, eld_mac, vehicle.vehicle_id, vehicle.vin, eld_sn, driver.full_name, gps_sn,
             random_trailer_gps[1], trailer.trailer_id))

    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'locations.txt'), 'r') as file:
        us_locations = file.readlines()
    file.close()
    shuffle(us_locations)
    locations_queue = Queue()
    [locations_queue.put((loc.split(',')[2].strip(), loc.split(',')[3].strip())) for loc in us_locations]

    used_vehicle_ids = []

    for email in drivers:
        device = random.choice([('Android', '9'), ('Android', '10'), ('Ios', '12.4.3'), ('Ios', '13.2.1')])
        device_id = rutil.string(size=12, chars=string.ascii_letters)

        headers = {}

        basic_auth_token = base64.b64encode(
            json.dumps({'username': email[0], 'password': 'seve'}).encode()).decode('utf-8')
        headers['AuthorizationCredentials'] = 'Basic {}'.format(basic_auth_token)
        headers['FleetmgVersion'] = '1'
        headers['apiVersion'] = '5'
        headers['content-type'] = 'application/json'

        payload = {'needNewToken': True}
        r = requests.post('{}/mob/login'.format(app_url), data=json.dumps(payload), headers=headers)
        if r.status_code == 408:
            print("User [{}] have multiple accounts!".format(email[0]))
            basic_auth_token = base64.b64encode(
                json.dumps({'username': email[0], 'password': 'seve', 'account': com.company_name})
                    .encode()).decode('utf-8')
            headers['AuthorizationCredentials'] = 'Basic {}'.format(basic_auth_token)
            r = requests.post('{}/mob/login'.format(app_url), data=json.dumps(payload), headers=headers)
        elif r.status_code != 200:
            print('Login failed [{}]!\n{}'.format(email[0], r.text))

        del headers['AuthorizationCredentials']
        headers['Authorization'] = 'Bearer {}'.format(r.json()['token'])
        requests.get('{}/mob/login'.format(app_url), data=json.dumps(payload), headers=headers)
        # driver_full_name = '{} {}'.format(r.json()['firstName'], r.json()['lastName'])

        payload = {'activityType': 'LOGIN', 'appType': device[0], 'appVersion': '1.13.2', 'apiVersion': '5',
                   'deviceId': device_id, 'notificationId': None, 'osVersion': device[1]}
        requests.post('{}/mob/activity/sync'.format(app_url), data=json.dumps(payload), headers=headers)

        r = requests.get('{}/mob/vehicle/search'.format(app_url), params={'searchString': 'V'},
                         headers=headers)
        vehicle_ids = [v['vehicleId'] for v in r.json()['vehicles'] if v['vehicleId'] not in used_vehicle_ids]
        random_vehicle_id = random.choice(vehicle_ids)

        requests.put('{}/mob/vehicle/assign/{}'.format(app_url, random_vehicle_id), headers=headers)
        used_vehicle_ids.append(random_vehicle_id)

        files = {
            'file': open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'driver_signature.png'), 'rb')}
        del headers['content-type']
        requests.post('{}/mob/signature'.format(app_url), headers=headers, files=files)
        headers['content-type'] = 'application/json'

        payload = {'cycleRule': 'USA_70_8', 'vehicleRule': 'Property', 'exceptionTypes': None,
                   'distanceUnit': 'MILE', 'mapMode': '0'}
        requests.post('{}/mob/settings'.format(app_url), data=json.dumps(payload), headers=headers)

        now = datetime.now(timezone.utc)
        epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
        posix_timestamp_micros = (now - epoch) // timedelta(microseconds=1)
        posix_timestamp_millis_now = posix_timestamp_micros // 1000 - random.choice([0, 5, 10, 15, 20, 25]) * 1000

        data = daily_log.initial_logs_json(c[0], f'{driver.first_name} {driver.last_name}',
                                           random_vehicle_id, email[2], device_id, email[1], trailer=email[8])
        requests.post('{}/mob/incompleteDailyLogs'.format(app_url), data=data, headers=headers)

        location = locations_queue.get()
        geo_data = GeometrisData(posix_timestamp_millis_now // 1000, str(now), email[3], email[4], location[0],
                                 location[1])
        reasons = []
        for _ in range(70):
            reasons.append('ON_PERIODIC')
        for _ in range(20):
            reasons.append('IDLING')
        for _ in range(5):
            reasons.append('POWER_CUT')
        for _ in range(5):
            reasons.append('DISCONN')
        geo_data.message_reason = random.choice(reasons)

        geo_data.faults = random.choice(
            [[], [], [], [], [], [], [], [], [], [], [], ['P22FE'], ['P1154', 'C2100', 'U116F']])
        if geo_data.message_reason == 'POWER_CUT' or geo_data.message_reason == 'DISCONN':
            geo_data.faults = []

        if geo_data.message_reason == 'IDLING':
            geo_data.speed = 0
            geo_data.obd_speed = 0

        driving_statuses = ['DRIVING', 'OFF_DUTY_PC', 'ON_DUTY_YM']
        non_driving_statues = ['ON_DUTY', 'OFF_DUTY', 'SLEEPER_BERTH']

        if geo_data.message_reason == 'ON_PERIODIC':
            if 0 <= geo_data.speed <= 10:
                last_status = random.choices(population=driving_statuses, weights=[0.1, 0.8, 0.4], k=1)[0]
            else:
                last_status = 'DRIVING'
            eld_status = 'CONNECTED'
        elif geo_data.message_reason == 'IDLING':
            last_status = random.choices(population=non_driving_statues, weights=[0.9, 0.6, 0.8], k=1)[0]
            eld_status = 'CONNECTED'
        elif geo_data.message_reason == 'POWER_CUT':
            last_status = random.choices(population=non_driving_statues, weights=[0.1, 0.2, 0.2], k=1)[0]
            eld_status = 'CONNECTED'
        elif geo_data.message_reason == 'DISCONN':
            last_status = random.choices(population=non_driving_statues, weights=[0.1, 0.6, 0.7], k=1)[0]
            eld_status = 'DISCONNECTED'
        else:
            last_status = 'OFF_DUTY'
            eld_status = 'DISCONNECTED'

        payload_alerts = {'driverState': last_status, 'eldStatus': eld_status,
                          'timeRemaining': {'LIMIT_CYCLE_TOMORROW': 4200, 'LIMIT_CYCLE': 4200, 'LIMIT_DRIVE': 660,
                                            'LIMIT_REST': 660, 'LIMIT_DUTY': 840, 'TIME_UNTIL_SHIFT_RESET': 480},
                          'driverStateStartTime': posix_timestamp_millis_now}
        requests.post('{}/mob/alerts'.format(app_url), data=json.dumps(payload_alerts), headers=headers)

        if last_status == 'OFF_DUTY':
            payload = {'activityType': 'LOGOUT', 'appType': device[0], 'appVersion': '1.13.2',
                       'deviceId': device_id, 'osVersion': device[1], 'totalEngineHours': 0.0,
                       'totalVehicleMiles': 0.0, 'vnaMacAddress': 0.0}
            requests.post('{}/mob/logout'.format(app_url), data=json.dumps(payload), headers=headers)

        geometris_session = requests.session()
        geometris_session.post('{}/geometris/sync'.format(app_url), data=json.dumps(geo_data, cls=GeometrisDataEncoder),
                               headers={'authorization': 'Kowy4Gs_3Pw', 'content-type': 'application/json'})
        geometris_session.close()

        any_trek_session = requests.session()
        gps_data = AnyTrekData(email[6], geo_data.location.geo_location.latitude,
                               geo_data.location.geo_location.longitude)
        gps_data.gps.speed = geo_data.speed
        gps_data.gps.degrees = geo_data.direction_degrees
        gps_data.statistic.original_event = 1 if gps_data.gps.speed > 0 else 0

        any_trek_session.post('{}/gps/sync'.format(app_url), headers={'content-type': 'application/json'},
                              data=json.dumps(gps_data, cls=AnyTrekDataEncoder))

        trailer_gps_data = AnyTrekData(email[7], float(geo_data.location.geo_location.latitude) + 0.000025,
                                       geo_data.location.geo_location.longitude)
        gps_data.gps.speed = geo_data.speed
        gps_data.gps.degrees = geo_data.direction_degrees
        gps_data.statistic.original_event = 1 if gps_data.gps.speed > 0 else 0
        any_trek_session.post('{}/gps/sync'.format(app_url), headers={'content-type': 'application/json'},
                              data=json.dumps(trailer_gps_data, cls=AnyTrekDataEncoder))
        any_trek_session.close()

        print('SN: {}, S: {}, LA: {}, LN: {}'.format(gps_data.id, gps_data.gps.speed, gps_data.gps.latitude,
                                                     gps_data.gps.longitude))

        sleep(2)
    print('Company {} populated with data!'.format(c[0]))


if __name__ == "__main__":

    if len(sys.argv) != 5:
        print(
            'Exactly four arguments expected: [ELD Rider URL] [Company Name] [Company Admin Mail] [Number of users '
            'with data]!')
        exit(1)

    args = list(sys.argv)[1:5]
    url = args[0]
    companies = {(args[1], args[2], int(args[3]))}

    # companies = {('Iske 24', 'imarkovic+24@lioneight.com', 24)}

    product = []
    for company in companies:
        product.append((url, company))

    agents = len(companies)
    with multiprocessing.Pool(processes=agents) as pool:
        pool.map(create_data, product)
